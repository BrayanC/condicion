#Programa que presenta una calificación cualitativa
#de acuerdo a ciertas equivalenicas.

# Autor: Brayan Gonzalo Cabrera Cabrera.

# brayan.cabrera@unl.edu.ec

try:

    puntuación = float (input("Introduzca puntuación:"))
    # validación del rango de la puntuación
    if puntuación >= 0 and puntuación <= 1.0:

        if puntuación >= 0.9:
            print("Sobresaliente")
        elif puntuación >= 0.8:
            print("Notable")
        elif puntuación >= 0.7:
            print("Bien")
        elif puntuación >= 0.6:
            print("Suficiente")
        elif puntuación < 0.6:
            print("Insuficiente")
    else:
        print("Puntuación Incorrecta")
except:
    print("Puntuación Incorrecta")